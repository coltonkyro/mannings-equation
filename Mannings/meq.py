import numpy as py


class mannings(object):
    """Implementation of Mannings Equation to Determine Stream Height from Discharge"""

    def __init__(self, w, s, n):
        """

        :param w: Width of the Channel (m)
        :param s: Slope of the Channel (dimensionless)
        :param n: Mannings coefficient (s/m^1/3)
        """

        ### Parameters

        self.w = w
        self.s = s
        self.n = n

        ### State Variables

        self.d = []  ### empty list for depth values to be put into

    def zero(self, Q, d):
        """
        Returns error value for a guessed height in Mannings Equation for discharge
        :param Q: Discharge (m3/s)
        :param d: Depth (m)
        :return: Error value, should be zero
        """
        solution = ((((((self.w * d) / (self.w * d + self.w)) ** (2/3)) * py.sqrt(self.s)) / self.n) / (self.w * d)) - Q
        return solution

    def dzero(self, Q, d):
        """
        Returns the slope at a point d in the equation that determines the error value for a given guessed height for Mannings Equation for discharge,
        :param Q: Discharge (m3/s)
        :param d: depth (m)
        :return: The slope of function zero
        """
        solution = ((py.sqrt(self.s) * self.w * d * ((self.w * d) / (self.w * d + self.w) ** (2 / 3))) / self.n) - Q
        return solution

    def solve_water_balance(self, Q):
        d = 0.5
        i = 0
        error = 1

        while (abs(error) > 1e-6) and (i < 500):
            error = self.zero(Q, d)
            derror = self.dzero(Q, d)
            d = d - error / derror
            i += 1

        self.d.append(d)



