import meq as mq

w = 2  # meters
s = 0.025  # unitless
n = 0.75   # Mannings coefficient (s/m^1/3)

model = mq.mannings(w,s,n)


print(model.zero(0.5, 0.5))

print(model.solve_water_balance(0.5))