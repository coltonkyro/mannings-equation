import numpy as py
import meq as mq
import nose

class TestManningsEquation(object):

    @classmethod
    def setup_class(cls):
        """this method is run once for each class before any tests are run"""
        cls.w = 2  # meters
        cls.s = 0.025  # unitless
        cls.n = 0.75   # Mannings coefficient (s/m^1/3)

    @classmethod
    def teardown_class(cls):
        """this method is run once for each class __after__ all tests are run"""
        pass

    def setUp(self):
        """this method is run once before __each__ tests is run"""
        pass

    def teardown(self):
        """this method is run once after __each__ tests is run"""
        pass

    def test_init(self):
        net = mq.mannings(self.w, self.s, self.n)
        nose.tools.assert_equal(net.s, self.s)
        nose.tools.assert_equal(net.w, self.w)
        nose.tools.assert_equal(net.n, self.n)
        for i in range(4):
            net.d.append(i+1)
        nose.tools.assert_equal(net.d, [1,2,3,4])

    def testzero(self):
        net = mq.mannings(self.w, self.s, self.n)

        d = 2

        solution = (((((self.w*d)/(self.w*d+self.w))**(2/3))*py.sqrt(self.s))/self.n)/(self.w*d)
        answer = net.zero(d)

        nose.tools.assert_almost_equal(solution, answer, places=4)

    def testdzero(self):
        net = mq.mannings(self.w, self.s, self.n)

        d = 2

        solution = (py.sqrt(self.s)*self.w*d*((self.w*d)/(self.w*d+self.w)**(2/3)))/self.n
        answer = net.dzero(d)
        nose.tools.assert_almost_equal(solution, answer, places=4)

    def test_solve_water_balance(self):

        net = mq.mannings(self.w, self.s, self.n)
        Q = 0.5

        solution = net.solve_water_balance(Q)
        nose.tools.assert_less_equal(abs(solution), 1e-6)

        print(net.solve_water_balance(Q))